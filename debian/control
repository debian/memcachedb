Source: memcachedb
Section: web
Priority: optional
Maintainer: Arto Jantunen <viiru@debian.org>
Standards-Version: 3.9.6.0
Build-Depends: debhelper (>= 9), libdb-dev, libevent-dev, dh-autoreconf
Build-Conflicts: autoconf2.13, automake1.4
Homepage: http://memcachedb.org/
Vcs-Browser: https://salsa.debian.org/debian/memcachedb/
Vcs-Git: https://salsa.debian.org/debian/memcachedb.git

Package: memcachedb
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, adduser
Recommends: db-util
Description: Persistent storage engine using the memcache protocol
 Memcachedb is a distributed key-value storage system designed for
 persistent data. It is NOT a cache solution, but a persistent storage
 engine for fast and reliable key-value based object storage and
 retrieval.
 .
 It conforms to the memcache protocol, so any memcached client can
 have connectivity with it. Memcachedb uses Berkeley DB as a storing
 backend, so lots of features including transactions and replication
 are available.
